<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
 
 <%@taglib uri="/struts-tags" prefix="s" %> 
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Perle di saggezza</title>
 
<s:head />
 </head>
<body>

<table align="center"  width="100%">
<tr>
<td align="center">
<s:form namespace="/" action="generateMessage">
<s:submit value="Genera" style="font-size : 22pt ; font-family : Comic sans MS ; color : black "/>
</s:form>
</td>
</tr>
<tr>
<td align="right">
<a href="/PerleSaggezza/apk/PerleSaggezza.apk"><img src="images/android-s.png" width="42" height="50" title="Download Android App" alt="Download Android App"></a>
<a href="https://telegram.me/sds4fun_perlesaggezza_bot"><img src="images/telegram-s.png" width="50" height="50" title="Telegram Bot" alt="Telegram Bot"></a>
</td>
</tr>
</table>
 
</body>
</html>