<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
</head>

<body>
<h1>Seleziona lo scenario che pi� ti piace</h1>

<s:form action="generateMessage" namespace="/">

<s:iterator value="scenarios">
<tr>
            <td><s:radio name="yourScenario" list="#{top.scenarioCode:''}" theme="simple"></s:radio></td>
		    <td><s:property value="scenarioValue"/></td>
</tr>
</s:iterator>

<s:submit value="Genera" />

</s:form>

</body>
</html>