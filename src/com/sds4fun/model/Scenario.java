package com.sds4fun.model;

public class Scenario{

	private int scenarioCode;
	private String scenarioValue;

	public Scenario(int scenarioCode, String scenarioValue) {
		this.scenarioCode = scenarioCode;
		this.scenarioValue = scenarioValue;
	}

	public int getScenarioCode() {
		return scenarioCode;
	}

	public void setScenarioCode(int scenarioCode) {
		this.scenarioCode = scenarioCode;
	}

	public String getScenarioValue() {
		return scenarioValue;
	}

	public void setScenarioValue(String scenarioValue) {
		this.scenarioValue = scenarioValue;
	}


}