package com.sds4fun.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
public class MessageGenerator {
	
	private static Logger logger = LoggerFactory.getLogger(MessageGenerator.class);
	
	private String message;
	private HashMap<Integer, String> hMessages = null;
	private HashMap<Integer, String> compagnie = null;
	private HashMap<Integer, String> luoghi = null;
	private HashMap<Integer, String> persone = null;
	private HashMap<Integer, String> azioni = null;
	private HashMap<Integer, String> citazioni = null;
	private HashMap<Integer, String> funzioni = null;
	private HashMap<Integer, String> cose = null;
	private HashMap<Integer, String> titolistudio = null;
	private HashMap<Integer, String> malattie = null;
	private HashSet<Integer> sCompagnie = null;   
	private HashSet<Integer> sLuoghi = null; 
	private HashSet<Integer> sPersone = null;
	private HashSet<Integer> sAzioni= null;
	private HashSet<Integer> sCitazioni = null;
	private HashSet<Integer> sFunzioni = null;
	private HashSet<Integer> sCose = null;
	private HashSet<Integer> sTitoliStudio = null;
	private HashSet<Integer> sMalattie = null;
	
	private int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}	
	
	private String getCompagnia() {
		int iC = (int) (Math.random()*compagnie.size()+1);
		while (sCompagnie.contains(iC))
			iC = (int) (Math.random()*compagnie.size()+1);
		sCompagnie.add(iC);
		return compagnie.get(iC);
	}
	private String getFunzione() {
		int iC = (int) (Math.random()*funzioni.size()+1);
		while (sFunzioni.contains(iC))
			iC = (int) (Math.random()*funzioni.size()+1);
		sFunzioni.add(iC);
		return funzioni.get(iC);
	}	
	private String getLuogo() {
		int iL = (int) (Math.random()*luoghi.size()+1);
		while (sLuoghi.contains(iL))
			iL = (int) (Math.random()*luoghi.size()+1);
		sLuoghi.add(iL);		
		return luoghi.get(iL);
	}
	
	private String getPersona() {
		int iP = (int) (Math.random()*persone.size()+1);
		while (sPersone.contains(iP))
			iP = (int) (Math.random()*persone.size()+1);
		sPersone.add(iP);
		return persone.get(iP);
	}	
	
	private String getAzione() {
		int iA = (int) (Math.random()*azioni.size()+1);
		while (sAzioni.contains(iA))
			iA = (int) (Math.random()*azioni.size()+1);
		sAzioni.add(iA);			
		return azioni.get(iA);
	}	

	private String getCitazione() {
		int iC = (int) (Math.random()*citazioni.size()+1);
		while (sCitazioni.contains(iC))
			iC = (int) (Math.random()*citazioni.size()+1);
		sCitazioni.add(iC);			
		return citazioni.get(iC);
	}
	
	private String getCosa() {
		int iC = (int) (Math.random()*cose.size()+1);
		while (sCose.contains(iC))
			iC = (int) (Math.random()*cose.size()+1);
		sCose.add(iC);			
		return cose.get(iC);
	}
	
	private String getTitoloStudio() {
		int iTS = (int) (Math.random()*titolistudio.size()+1);
		while (sTitoliStudio.contains(iTS))
			iTS = (int) (Math.random()*titolistudio.size()+1);
		sTitoliStudio.add(iTS);			
		return titolistudio.get(iTS);
	}
	
	private String getMalattie() {
		int iM = (int) (Math.random()*malattie.size()+1);
		while (sMalattie.contains(iM))
			iM = (int) (Math.random()*malattie.size()+1);
		sMalattie.add(iM);			
		return malattie.get(iM);
	}	

	private int getAnno() {
		return (int) (Math.random()*50)+1950;
	}
	
	/**
	 * Restituisce il numero degli scenari gestiti
	 */
	public int scenarioSize() {
		//logger.info("scenarioSize");
		loadFiles();
    	resetDuplicateManager();		
		scenarioCollection();
		return hMessages.size();
	}	
	
	
	/**
	 * Restituisce tutti gli scenari
	 */
	public HashMap<Integer, String> allScenarios() {
		//logger.info("allScenarios");
		loadFiles();
    	resetDuplicateManager();		
		scenarioCollection();
    	return hMessages;
	}
	
	/**
	 * Restituisce uno scenario, in funzione dell'indice i oppure ne estrae uno casualmente dall'elenco
	 */
	public String generateScenario(int i) {
		//logger.info("generateScenario: "+i);
		loadFiles();
    	resetDuplicateManager();		
		scenarioCollection();
    	if(i>0 && i<=hMessages.size()) {
    		message = hMessages.get(i);
    	}else {
    		message = hMessages.get((int) (Math.random()*hMessages.size()+1));
    	}
    	return message;
	}
	
	private String capitalize(String str) {
		String cap = str.substring(0, 1).toUpperCase() + str.substring(1);
		return cap;
	}
	/**
	 * 
	 */
	private void scenarioCollection() {
		//logger.info("scenarioCollection");
		hMessages = new HashMap<Integer, String>();
    	hMessages.put(1, "L'ho detto anche a "+getPersona()+": questo � l'unico modo in cui possa essere fatta la "+getCompagnia()+", fanno cos� "+getPersona()+" a "+getLuogo()+" e "+getPersona()+" a "+getLuogo()+" sin dal "+getAnno()+" e non vedo perch� dobbiamo inventarci delle altre cose.");
    	hMessages.put(2, "Non dobbiamo ripetere l'errore dei miei predecessori, "+getPersona()+" e "+getPersona()+". E poi, qualsiasi cosa scelga "+getPersona()+" non sar� un problema nel fare la "+getCompagnia()+" perch� "+getPersona()+" era il mio vice nella "+getCompagnia()+", mentre ho assunto "+getPersona()+" nella "+getCompagnia()+" nel "+getAnno()+".");
    	hMessages.put(3, "Nel "+getAnno()+", a "+getLuogo()+", eravamo io, "+getPersona()+" e "+getPersona()+" ed abbiamo lanciato la "+getCompagnia()+".");
    	hMessages.put(4, "Il progetto dell'anno sara' gestito in modalita' adopt, prendendo esempio dalla "+getCompagnia()+" e dalla "+getCompagnia()+". Dobbiamo fare quello che fanno gli altri senza inventarci nulla, come consigliato da "+getPersona()+" e "+getPersona()+" nel "+getAnno()+" durante la conferenza di "+getLuogo()+".");
    	hMessages.put(5, "Io e "+getPersona()+" siamo stati chiamati da "+getPersona()+" per rimodernare la "+getCompagnia()+". Fatte le opportune valutazioni, nel "+getAnno()+" abbiamo trasferito la sede a "+getLuogo()+" trasformandola nella "+getCompagnia()+" con grande successo.");
    	hMessages.put(6, "Mentre andavo in metro a "+getLuogo()+" nella calca della stazione ho incontrato "+getPersona()+". Abbiamo parlato della situazione della "+getCompagnia()+". Lo scenario � lo stesso della "+getCompagnia()+" nel "+getAnno()+", dobbiamo rapidamente aprire un tavolo e fare un ragionamento con "+getPersona()+".");
    	hMessages.put(7, "Il PM del progetto per il lancio della " + getCompagnia() + " sar� " + getPersona() + ". Convochiamo subito una riunione a " + getLuogo()+ " con " + getPersona() + " e " + getPersona() + ", non possiamo rischiare di fallire come la " + getCompagnia() + " nel " + getAnno() + " a " + getLuogo() + ".");
    	hMessages.put(8, "Come concordato ieri con " + getPersona() + ", dobbiamo avviare quanto prima il tavolo di lavoro con " + getPersona() + " per stabilire il piano di rollout della " + getCompagnia() + ". Nelle more di tale implementazione il project manager sar� " + getPersona() + " che gi� nel " + getAnno() + " aveva contribuito al lancio della " + getCompagnia() + ".");
    	hMessages.put(9, "Stiamo spendendo troppo per fare la " + getCompagnia() + ", del resto "+ getPersona() + " � un incompetente mezzo amico di quel ladro di " + getPersona() + ". Non rimane che sequestrare il badge come avevo gi� fatto nel " + getAnno() + " a " + getPersona()+ " quando lavoravo alla " + getCompagnia() + "."); 
    	hMessages.put(10,  getPersona() + " e " + getPersona() + " a " + getLuogo() + ", parlando della " + getCompagnia() + ", " + getAzione() + " e giungono alla conclusione che: << " + getCitazione() + ">>.");
    	hMessages.put(11,  "In questo momento il progetto per il lancio della " + getCompagnia() +" � fermo. A questo punto occorre fare un passaggio con " + getFunzione() + "; nel mentre che " + getPersona() + " e " + getPersona() + " " +getAzione() + ", chiamiamo un incontro a " + getLuogo() + ". Ho fatto lo stesso nel " + getAnno() + " quando mandando " + getPersona() + " a parlare con " + getFunzione() + " sono riuscito a sbloccare la situazione decretando il successo della " + getCompagnia() + ". ");
    	hMessages.put(12,  "Si � verificato un nuovo problema: questo per me � l'ultimo! " + getPersona() + " e " + getPersona() + " " +getAzione() + ", mentre " + getPersona() + " distrugge la " + getCompagnia() + "! Parlate con "+ getPersona()+" e fate come deciso nel " + getAnno() + " a "+getLuogo()+ " quando ho realizzato la "+getCompagnia());
    	hMessages.put(13,  "Per questo problema ho gi� chiesto la testa di "+getPersona() +". Non a caso nel "+getAnno()+" gli ho consigliato di andare a vendere "+getCosa()+". D'altra parte non essendo laureato in "+getTitoloStudio()+" e non avendo lavorato nella "+getCompagnia()+", non mi stupisco che abbia fatto fallire il lancio della "+getCompagnia());
    	hMessages.put(14,  "Per approfondire l'argomento potete scaricare l'articolo scritto da "+getPersona() +" nel "+getAnno() +" che parla di "+getTitoloStudio()+" con un interessante approfondimento di "+getPersona()+".");
    	hMessages.put(15,  "Come disse "+getPersona() +" nel "+getAnno()+" a "+getLuogo()+" :<<"+getCitazione()+">>. E' esattamente quello che accadde alla "+getCompagnia()+" per questo dobbiamo subito smettere di sprecare "+getCosa()+".");
    	hMessages.put(16,  "Nel "+getAnno()+", a "+getLuogo()+", eravamo io, "+getPersona()+" e "+getPersona()+". Mentre compravamo "+getCosa() +" abbiamo chiaramente sentito "+getPersona() +" esclamare:<<" +getCitazione() +" >>.");
    	hMessages.put(17,  "Dopo aver visto la pubblicit� della "+getCompagnia()+" mia moglie � immediatamente andata al centro commerciale di "+getLuogo()+" per comprare "+getCosa()+". Ha incontrato "+getPersona()+" e "+getPersona() +" che "+getAzione()+" e non � pi� tornata ...");
    	hMessages.put(18,  "Uhh guarda "+getPersona()+", mo' lo chiamo ...");    	
    	hMessages.put(19,  "Dobbiamo parlare con "+getPersona()+", lui nel "+getAnno()+" ha fatto fare il salto di qualit� alla "+ getCompagnia() +" perch� come "+getPersona()+" � uno che vede scorrere la corrente e non uno che cavalca l'onda.");
    	hMessages.put(20,  "Io e "+getPersona()+", che � un visionario, abbiamo visto nascere la "+ getCompagnia() +". Quanta tecnologia hai visto morire nel frattempo? Hai visto che fine ha fatto la "+getCompagnia()+"? Ora tutti dicono le stesse cose che vado dicendo dal "+getAnno()+".");
    	hMessages.put(21,  "Vorrei che uscissimo da questa logica secondo la quale: \"" +getCitazione()+"\". Dobbiamo disambiguare cose quali la " + getCosa() + " che non necessitano di analisi. Insieme con " + getPersona() + " possiamo capire cosa ci serve");
    	hMessages.put(22,  "Per me " +getFunzione()+" e' come il centrocampo di una squadra di calcio; ecco perche' ho voluto il team di "+ getPersona() + " direttamente con me. D'altra parte, tutti gli IT manager del mercato hanno lavorato con me, e' una piccola mafia. Come sapete, il nostro slogan e' \"Semplici come la " + getCompagnia() + ", efficienti come la " + getCompagnia() + " e con l'attenzione al cliente come la " + getCompagnia());
    	hMessages.put(23,  "I consulenti che non voglio pi� vedere, faccio nomi e cognomi, sono " +getPersona()+" e " + getPersona() + ", sono quelli della " + getCompagnia() + ", perch� non sono bravi, anzi ne sanno meno di voi. Infatti voi siete mediamente bravi, anche con delle eccellenze.");
    	hMessages.put(24,  capitalize(getFunzione())+" ci chiede di fare la "+getCompagnia()+" 2.0: avete presente la pubblicit� della " +getCompagnia()+ "? Ecco dobbiamo fare la stessa cosa. Partiamo dal documento redatto nel "+getAnno()+ " a "+ getLuogo() + " da "+getPersona()+".");
    	hMessages.put(25,  "Per fare la "+getCompagnia()+", ho gi� chiesto il favore a "+getPersona()+" e "+getPersona()+" di fare una slide.");
    	hMessages.put(26,  "Dobbiamo fare gruppo con "+getPersona()+" e "+getPersona()+" per far capire cosa facciamo noi. Se continuiamo a parlare di "+getCosa()+" non lo capiranno mai e diranno che facciamo sempre noi. "+getPersona()+ " non sta capendo.");
    	hMessages.put(27,  "L'unica cosa che vuole fare "+getPersona()+" � la "+getCompagnia()+". Facciamo una task force con "+getPersona()+ " e "+getPersona()+ ".");
    	hMessages.put(28,  "Uhh guarda "+getPersona()+", ora chiamo "+getPersona());
    	hMessages.put(29,  "Inziate la riunione con "+getFunzione()+" perch� sto andando al "+getRandomNumberInRange(2,7)+"� piano con "+getPersona()+" per parlare della "+getCompagnia());
    	hMessages.put(30,  getPersona()+", se nell'email trovo ancora il nome di "+getPersona().trim()+", non pagher� pi� le fatture di "+getCompagnia()+"!");
    	hMessages.put(31,  "E' come parlare con "+getFunzione().trim()+" ed avere paura della "+getMalattie()+"!");
    	String sPersona = getPersona().trim();
    	hMessages.put(32,  sPersona+"! "+sPersona+"!! "+sPersona+"!!!");
	}

	/**
	 * Effettua il reset delle strutture dati necessarie per evitare la riproposizione della stessa entit� per scenario
	 */
	private void resetDuplicateManager() {
		//logger.info("resetDuplicateManager");
		sCompagnie = new HashSet<Integer>();   
    	sLuoghi = new HashSet<Integer>(); 
    	sPersone = new HashSet<Integer>();  
    	sAzioni = new HashSet<Integer>();
    	sCitazioni = new HashSet<Integer>();
    	sCose = new HashSet<Integer>();
    	sTitoliStudio = new HashSet<Integer>();
    	sMalattie = new HashSet<Integer>();
    	sFunzioni = new HashSet<Integer>();
	}
	
	/**
	 * Consente di caricare i file con le liste delle entit� utilizzate negli scenari 
	 */
	private void loadFiles() {
		//logger.info("loadFiles");
		compagnie = FileReaders.readFileToHashMap("compagnie.txt");
    	luoghi = FileReaders.readFileToHashMap("luoghi.txt");
    	persone = FileReaders.readFileToHashMap("persone.txt");
    	azioni = FileReaders.readFileToHashMap("azioni.txt");
    	citazioni = FileReaders.readFileToHashMap("citazioni.txt");
    	funzioni = FileReaders.readFileToHashMap("funzioni.txt");
    	cose = FileReaders.readFileToHashMap("cose.txt");
    	titolistudio = FileReaders.readFileToHashMap("titolistudio.txt");
    	malattie = FileReaders.readFileToHashMap("malattie.txt");
	}
	
	public static void main(String[] args) {
		MessageGenerator mg = new MessageGenerator();
		logger.info(mg.generateScenario(29));
	}
}
