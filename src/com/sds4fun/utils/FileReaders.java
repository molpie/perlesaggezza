package com.sds4fun.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class FileReaders {
	
	  public static HashMap<Integer, String> readFileToHashMap(String fileName) {
		  HashMap<Integer, String> map = new HashMap<Integer, String>();
		  InputStream initialStream = null;
		  BufferedReader reader = null;

			try {

				initialStream = FileReaders.class.getClassLoader().getResourceAsStream("./data/"+fileName);
//		        String filePath = (FileReaders.class.getClassLoader().getResource("./data/")).getFile()+fileName;
//		        logger.info(filePath);
				reader = new BufferedReader(new InputStreamReader(initialStream));
				String line;
				int i = 1;
			    while ((line = reader.readLine()) != null){
			    	map.put(i, line);
			    	i++;
			    }				

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (initialStream != null) {
					try {
						initialStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}				
			}
			return map;	
		  }
	  
}
