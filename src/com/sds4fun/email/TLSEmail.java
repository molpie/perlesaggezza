package com.sds4fun.email;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import com.sds4fun.utils.FileReaders;
import com.sds4fun.utils.MessageGenerator;

public class TLSEmail {
	
	private static Logger logger = LoggerFactory.getLogger(TLSEmail.class);
	
	private HashMap<Integer, String> email = null;

	/**
	   Outgoing Mail (SMTP) Server
	   requires TLS or SSL: smtp.gmail.com (use authentication)
	   Use Authentication: Yes
	   Port for TLS/STARTTLS: 587
	 */
	public static void main(String[] args) {
		TLSEmail tlse = new TLSEmail();
		tlse.sendTLSEmail();
		
	}

	public void sendTLSEmail() {
		email = FileReaders.readFileToHashMap("email.txt");
		final String fromEmail = "sds.for.fun@gmail.com"; //requires valid gmail id
		final String password = "PerleSaggezza.1"; // correct password for gmail id
//		final String toEmail = "pietro.molinaro@gmail.com"; // can be any email id 
		
		logger.info("TLSEmail Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
        MessageGenerator mg = new MessageGenerator();
        String answer = mg.generateScenario(0);
		
	    Iterator<Entry<Integer, String>> it = email.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, String> pair = (Map.Entry<Integer, String>)it.next();
	        logger.info(pair.getKey() + " = " + pair.getValue());
	        EmailUtil.sendEmail(session, (String)pair.getValue(),"La perla del giorno", answer);
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}	
	
}