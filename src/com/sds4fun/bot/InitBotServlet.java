package com.sds4fun.bot;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.BotSession;

/**
 * Servlet implementation class MainServlet
 */
public class InitBotServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TelegramBotsApi botsApi = null;
	private BotSession botSession = null;
	
	private static Logger logger = LoggerFactory.getLogger(InitBotServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitBotServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("init");
	    telegramBotLoader();	
	}

	private void telegramBotLoader() {
		logger.info("telegramBotLoader");
	      try {
		      // Instantiate Telegram Bots API
	    	  botsApi  = new TelegramBotsApi(DefaultBotSession.class);

	    	  // Register our bot
	    	  botSession = botsApi.registerBot(new MyAmazingBot());
	      } catch (TelegramApiException e) {
	          e.printStackTrace();
	      }
	      logger.info("MyAmazingBot successfully started!");
	}

	@Override
	public void destroy() {
		logger.info("destroy");
		botSession.stop();
		super.destroy();		
	}

}
