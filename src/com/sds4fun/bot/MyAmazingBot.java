package com.sds4fun.bot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sds4fun.utils.MessageGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MyAmazingBot extends TelegramLongPollingBot {
	private static Logger logger = LoggerFactory.getLogger(MyAmazingBot.class);
	
	private void log(String first_name, String last_name, String user_username, String user_id, String txt, String bot_answer) {
        logger.info("\n ----------------------------");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        logger.info(dateFormat.format(date));
        logger.info("Message from " + first_name + " " + last_name + " (user_username = " + user_username + "). (id = " + user_id + ") \n Text - " + txt);
        logger.info("Bot answer: \n Text - " + bot_answer);
    }
	
   @Override
   public void onUpdateReceived(Update update) {
	    // We check if the update has a message and the message has text
	    if (update.hasMessage() && update.getMessage().hasText()) {
	        // Set variables
	        String user_first_name = update.getMessage().getChat().getFirstName();
	        String user_last_name = update.getMessage().getChat().getLastName();
	        String user_username = update.getMessage().getChat().getUserName();
	        long user_id = update.getMessage().getChat().getId();
	        String message_text = update.getMessage().getText();
	        long chat_id = update.getMessage().getChatId();

	        MessageGenerator mg = new MessageGenerator();
	        String answer = mg.generateScenario(0);

	        SendMessage message = SendMessage
						            .builder()
						            .chatId(chat_id)
						            .text(answer)
						            .build();
	        log(user_first_name, user_last_name, user_username, Long.toString(user_id), message_text, answer);
	        try {
//	            sendMessage(message); // Sending our message object to user
	        	execute(message); // Sending our message object to user
	        } catch (TelegramApiException e) {
	            e.printStackTrace();
	        }
	    }
	}


   @Override
   public String getBotUsername() {
       // Return bot username
       // If bot username is @MyAmazingBot, it must return 'MyAmazingBot'
       return "sds4fun_perlesaggezza_bot";
   }

   @Override
   public String getBotToken() {
       // Return bot token from BotFather
       return "897557897:AAHrmvNLBnZfguvKX2Gn69zAIs7q1Eo0fBg";
   }
}
