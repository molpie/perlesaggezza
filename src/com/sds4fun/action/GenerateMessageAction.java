package com.sds4fun.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.sds4fun.utils.MessageGenerator;
 
public class GenerateMessageAction extends ActionSupport implements SessionAware {
 
	private static final long serialVersionUID = -4099960795628030467L;
	
	private String message;
	private String yourScenario;
	private HashMap<Integer, String> hMessages = null;
    private Map<String, Object> sessionMap = null;
    private boolean[] bMessages = null;
    
    @Override
    public void setSession(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
    
    private static boolean areAllTrue(boolean[] array)
    {
        for(boolean b : array) if(!b) return false;
        return true;
    }
    
    private void retrieveBMessages() {
    	bMessages = (boolean[]) sessionMap.get("bMessages");
    	if (bMessages==null) {
    		bMessages = new boolean[hMessages.size()];
    	}
    }
    
    private void storeBMessages() {
    	sessionMap.put("bMessages",bMessages);
    }    
	
	private void assignMessage() {
		retrieveBMessages();
		if (areAllTrue(bMessages))
			bMessages = new boolean[hMessages.size()];
		int iC = (int) (Math.random()*hMessages.size()+1);
		while (bMessages[iC-1])
			iC = (int) (Math.random()*hMessages.size()+1);
		message = hMessages.get(iC);
		bMessages[iC-1] = true;
		storeBMessages();
/*		
		logger.info("iC: "+iC);
		System.out.print("[");
		for (int i = 0;i<bMessages.length;i++) {
			System.out.print(bMessages[i]);
			if (i<bMessages.length-1)
				System.out.print(",");
		}
		logger.info("]");
*/		
	}


    public String execute() {
    	MessageGenerator mg = new MessageGenerator();
    	int iScenario = 0;    	
    	String sScenario = (String) sessionMap.get("sScenario");
    	if (yourScenario == null) {
	    	if (sScenario == null) {
	    		yourScenario = "0";
	    	}
	    	else {
	    		yourScenario = sScenario;
	    	}
    	}
    	try {
    		iScenario = Integer.parseInt(yourScenario);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	if (iScenario != 0) {
    		message = mg.generateScenario(iScenario);
    	}
    	else {
    		hMessages = mg.allScenarios();
    		assignMessage();
    	}
    	sessionMap.put("sScenario", yourScenario);
        return SUCCESS;
    }
    
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }

    public void validate() {
    }

	public String getYourScenario() {
		return yourScenario;
	}

	public void setYourScenario(String yourScenario) {
		this.yourScenario = yourScenario;
	}
  
}
