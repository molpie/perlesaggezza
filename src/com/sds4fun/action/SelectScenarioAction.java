package com.sds4fun.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sds4fun.model.Scenario;
import com.sds4fun.utils.MessageGenerator;
import com.opensymphony.xwork2.ActionSupport;

public class SelectScenarioAction extends ActionSupport{

	private static final long serialVersionUID = 2008211051021857720L;
	private List<Scenario> scenarios;
	private HashMap<Integer, String> hMessages = null;

	public SelectScenarioAction(){
		scenarios = new ArrayList<Scenario>();
		MessageGenerator mg = new MessageGenerator();
		hMessages = mg.allScenarios();
		scenarios.add(new Scenario(0,"Tutti"));
		for (int i = 1; i<=hMessages.size(); i++)
			scenarios.add(new Scenario(i,hMessages.get(i)));
	}

	public String getDefaultScenarioValue(){
		return "UNKNOWN";
	}

	public String execute() {
		return SUCCESS;
	}
	public String display() {
		return NONE;
	}

	public List<Scenario> getScenarios() {
		return scenarios;
	}

	public void setScenarios(List<Scenario> scenarios) {
		this.scenarios = scenarios;
	}

}
