package com.sds4fun.action;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

public class UploadFileAction extends ActionSupport implements ServletRequestAware {

	private static final long serialVersionUID = 4912117958034057442L;
    private File toBeUploaded;
    private String toBeUploadedFileName;
    private String toBeUploadedContentType;
    public String execute()
    {
         String filePath = (UploadFileAction.class.getClassLoader().getResource("./data/")).getFile();
//         logger.info(filePath);
         if ((toBeUploadedFileName !=null)&&
         	  (toBeUploadedFileName.equalsIgnoreCase("azioni.txt")||
        		 toBeUploadedFileName.equalsIgnoreCase("citazioni.txt")||
        		 	toBeUploadedFileName.equalsIgnoreCase("compagnie.txt")||
        		 		toBeUploadedFileName.equalsIgnoreCase("cose.txt")||
        		 			toBeUploadedFileName.equalsIgnoreCase("funzioni.txt")||
        		 				toBeUploadedFileName.equalsIgnoreCase("luoghi.txt")||
        		 					toBeUploadedFileName.equalsIgnoreCase("persone.txt") ||
        		 						toBeUploadedFileName.equalsIgnoreCase("email.txt") ||
        		 							toBeUploadedFileName.equalsIgnoreCase("titolistudio.txt"))) {
	         File fileToCreate = new File(filePath, this.toBeUploadedFileName);
	
	         try {
	            FileUtils.copyFile(this.toBeUploaded, fileToCreate);
	        } catch (IOException e) {
	            addActionError(e.getMessage());
	        }
	        return SUCCESS;
         }else
        	 return ERROR;
    }
    public File getToBeUploaded() {
        return toBeUploaded;
    }
    public void setToBeUploaded(File toBeUploaded) {
        this.toBeUploaded = toBeUploaded;
    }
    public String getToBeUploadedFileName() {
        return toBeUploadedFileName;
    }
    public void setToBeUploadedFileName(String toBeUploadedFileName) {
        this.toBeUploadedFileName = toBeUploadedFileName;
    }
    public String getToBeUploadedContentType() {
        return toBeUploadedContentType;
    }
    public void setToBeUploadedContentType(
            String toBeUploadedFileNameContentType) {
        this.toBeUploadedContentType = toBeUploadedFileNameContentType;
    }
    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        
    }
    
}