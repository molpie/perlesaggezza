package com.sds4fun.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sds4fun.email.TLSEmail;

public class EmailJob implements Job {
	
	private static Logger logger = LoggerFactory.getLogger(EmailJob.class);
	 
	@Override
    public void execute(JobExecutionContext context)
     throws JobExecutionException {
 
    	logger.info("Sono un Job e sono partito.");
		TLSEmail tlse = new TLSEmail();
		tlse.sendTLSEmail();
 
    }

 
}